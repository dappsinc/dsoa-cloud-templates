# Google Provider for DSOA

provider "google" {
  project = "dsoa-28"
  region  = "us-central1"
  zone    = "us-central1-c"
}


# Kubernetes Cluster

resource "google_container_cluster" "primary" {
  name               = "dsoa-cluster"
  zone               = "us-central1-a"
  initial_node_count = 3

}

  # The following outputs allow authentication and connectivity to the GKE Cluster.
output "client_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.primary.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.cluster_ca_certificate}"
}



# Compute Resource

resource "google_compute_instance" "vm_instance" {
  name         = "dsoa-instance"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "dappsinc/dsoa:latest"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "${google_compute_network.vpc_network.self_link}"
    access_config = {
    }
  }
}


# Google Network 

resource "google_compute_network" "vpc_network" {
  name                    = "dsoa-network"
  auto_create_subnetworks = "true"
}

